﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PANELINHA_LIGHT.DB.Base.Funcionario;

namespace PANELINHA_LIGHT
{
    public partial class RegistrarFuncionario : UserControl
    {
        public RegistrarFuncionario()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Botao_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Funcionario = txtfuncionario.Text;
                dto.Cargo = txtcargo.Text;
                dto.Tempo_trabalho = txttempotrab.Text;
                dto.Salario = txtsalario.Text;
                dto.Data_Nascimento = txtdata.Text;

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("O Funcionario foi Salvo com sucesso.");
            }
            catch
            {

                MessageBox.Show("ERRO: Preencha todos os Campos.");
            }

        }
    }
}

