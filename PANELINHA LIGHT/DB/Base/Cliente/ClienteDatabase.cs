﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PANELINHA_LIGHT.DB.Base.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script =
                @"insert into Cliente (Nome,Profissao,Endereco,Numero,Cidade,Cpf,Telefone)
	                                  values (@Nome,@Profissao,@Endereco,@Numero,@Cidade,@Cpf,@Telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Profissao", dto.Profissao));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("Numero", dto.Numero));
            parms.Add(new MySqlParameter("Cidade", dto.Cidade));
            parms.Add(new MySqlParameter("Cpf", dto.Cpf));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ClienteDTO> Listar()
        {
            string script = "select * from Cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();

                dto.Nome = reader.GetString("Nome");
                dto.Profissao = reader.GetString("Profissao");
                dto.Endereco = reader.GetString("Endereco");
                dto.Numero = reader.GetDecimal("Numero");
                dto.Cidade = reader.GetString("Cidade");
                dto.Cpf = reader.GetDecimal("Cpf");
                dto.Telefone = reader.GetDecimal("Telefone");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
    }
}