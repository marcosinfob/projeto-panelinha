﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PANELINHA_LIGHT.DB.Base.Cliente
{
    class ClienteDTO
    {
        public string Nome { get; set; }
        public string Profissao { get; set; }
        public string Endereco { get; set; }
        public decimal Numero { get; set; }
        public string Cidade { get; set; }
        public decimal Cpf { get; set; }
        public decimal Telefone { get; set; }




    }
}
