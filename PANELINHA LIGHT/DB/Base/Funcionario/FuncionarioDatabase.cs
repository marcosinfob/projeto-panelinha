﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PANELINHA_LIGHT.DB.Base.Funcionario
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script =
                @"insert into Funcionario (funcionario,cargo,tempo_trabalho,salario,data_nascimento)
	                                  values (@funcionario,@cargo,@tempo_trabalho,@salario,@data_nascimento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Funcionario", dto.Funcionario));
            parms.Add(new MySqlParameter("Cargo", dto.Cargo));
            parms.Add(new MySqlParameter("Tempo_trabalho", dto.Tempo_trabalho));
            parms.Add(new MySqlParameter("salario", dto.Salario));
            parms.Add(new MySqlParameter("data_nascimento", dto.Data_Nascimento));
           
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FuncionarioDTO> Listar()
        {
            string script = "select * from Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();

                dto.Funcionario = reader.GetString("Funcionario");
                dto.Cargo = reader.GetString("Cargo");
                dto.Tempo_trabalho = reader.GetString("Tempo_trabalho");
                dto.Salario = reader.GetString("salario");
                dto.Data_Nascimento = reader.GetString("data_nascimento");
                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
    }
}
