﻿using PANELINHA_LIGHT.DB.Base.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PANELINHA_LIGHT
{
    public partial class TelaLogin : Form
    {
        public TelaLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Contato_Click(object sender, EventArgs e)
        {
            {
                string login = txtlogin.Text;
                string senha = txtsenha.Text;


                LoginBusiness busi = new LoginBusiness();
                bool logou = busi.logar(login, senha);
                if (logou == true)
                {
                    Tela_Entrada usuario = new Tela_Entrada();
                    usuario.Show();
                    this.Hide();
                }


                else
                {
                    MessageBox.Show("verifique se login e/ou senha estão corretos!", "login", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}
