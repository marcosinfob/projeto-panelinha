﻿namespace PANELINHA_LIGHT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.telaInicial2 = new PANELINHA_LIGHT.TelaInicial();
            this.listarFuncionario1 = new PANELINHA_LIGHT.ListarFuncionario();
            this.registrarFuncionario1 = new PANELINHA_LIGHT.RegistrarFuncionario();
            this.cliente1 = new PANELINHA_LIGHT.Cliente();
            this.telaInicial1 = new PANELINHA_LIGHT.TelaInicial();
            this.listarPedido1 = new PANELINHA_LIGHT.Listartudo();
            this.estoque1 = new PANELINHA_LIGHT.Estoque();
            this.contato1 = new PANELINHA_LIGHT.contato();
            this.userControl12 = new PANELINHA_LIGHT.UserControl1();
            this.userControl11 = new PANELINHA_LIGHT.UserControl1();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.FUN_LIST = new System.Windows.Forms.Button();
            this.Funcionario = new System.Windows.Forms.Button();
            this.BotaoCliente = new System.Windows.Forms.Button();
            this.BotaoListar = new System.Windows.Forms.Button();
            this.BotaoEstoque = new System.Windows.Forms.Button();
            this.botao3 = new System.Windows.Forms.Button();
            this.Botao = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Pink;
            this.panel1.Controls.Add(this.FUN_LIST);
            this.panel1.Controls.Add(this.Funcionario);
            this.panel1.Controls.Add(this.BotaoCliente);
            this.panel1.Controls.Add(this.BotaoListar);
            this.panel1.Controls.Add(this.BotaoEstoque);
            this.panel1.Controls.Add(this.userControl11);
            this.panel1.Controls.Add(this.botao3);
            this.panel1.Controls.Add(this.Botao);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(-5, -5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 523);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Beige;
            this.panel2.Location = new System.Drawing.Point(206, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(515, 27);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Pink;
            this.panel3.Controls.Add(this.button5);
            this.panel3.Controls.Add(this.button4);
            this.panel3.Location = new System.Drawing.Point(194, -61);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(521, 105);
            this.panel3.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Pink;
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Location = new System.Drawing.Point(199, -3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(181, 141);
            this.panel5.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(386, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Bem Vindo!";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Monotype Corsiva", 48F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(383, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(305, 79);
            this.label3.TabIndex = 7;
            this.label3.Text = "Bem Vindo!";
            // 
            // telaInicial2
            // 
            this.telaInicial2.Location = new System.Drawing.Point(198, 143);
            this.telaInicial2.Name = "telaInicial2";
            this.telaInicial2.Size = new System.Drawing.Size(518, 358);
            this.telaInicial2.TabIndex = 13;
            // 
            // listarFuncionario1
            // 
            this.listarFuncionario1.Location = new System.Drawing.Point(201, 141);
            this.listarFuncionario1.Name = "listarFuncionario1";
            this.listarFuncionario1.Size = new System.Drawing.Size(511, 358);
            this.listarFuncionario1.TabIndex = 12;
            // 
            // registrarFuncionario1
            // 
            this.registrarFuncionario1.Location = new System.Drawing.Point(199, 144);
            this.registrarFuncionario1.Name = "registrarFuncionario1";
            this.registrarFuncionario1.Size = new System.Drawing.Size(511, 358);
            this.registrarFuncionario1.TabIndex = 11;
            // 
            // cliente1
            // 
            this.cliente1.Location = new System.Drawing.Point(201, 148);
            this.cliente1.Name = "cliente1";
            this.cliente1.Size = new System.Drawing.Size(511, 383);
            this.cliente1.TabIndex = 10;
            // 
            // telaInicial1
            // 
            this.telaInicial1.Location = new System.Drawing.Point(194, 143);
            this.telaInicial1.Name = "telaInicial1";
            this.telaInicial1.Size = new System.Drawing.Size(522, 368);
            this.telaInicial1.TabIndex = 9;
            // 
            // listarPedido1
            // 
            this.listarPedido1.Location = new System.Drawing.Point(201, 148);
            this.listarPedido1.Name = "listarPedido1";
            this.listarPedido1.Size = new System.Drawing.Size(511, 354);
            this.listarPedido1.TabIndex = 8;
            // 
            // estoque1
            // 
            this.estoque1.Location = new System.Drawing.Point(201, 144);
            this.estoque1.Name = "estoque1";
            this.estoque1.Size = new System.Drawing.Size(511, 358);
            this.estoque1.TabIndex = 6;
            // 
            // contato1
            // 
            this.contato1.Location = new System.Drawing.Point(201, 148);
            this.contato1.Name = "contato1";
            this.contato1.Size = new System.Drawing.Size(511, 344);
            this.contato1.TabIndex = 5;
            // 
            // userControl12
            // 
            this.userControl12.Location = new System.Drawing.Point(201, 148);
            this.userControl12.Name = "userControl12";
            this.userControl12.Size = new System.Drawing.Size(511, 344);
            this.userControl12.TabIndex = 4;
            // 
            // userControl11
            // 
            this.userControl11.Location = new System.Drawing.Point(337, 303);
            this.userControl11.Name = "userControl11";
            this.userControl11.Size = new System.Drawing.Size(380, 354);
            this.userControl11.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PANELINHA_LIGHT.Properties.Resources.oie_transparent__1_;
            this.pictureBox1.Location = new System.Drawing.Point(5, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(176, 139);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_divisa_para_baixo_45;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button5.Location = new System.Drawing.Point(426, 55);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(44, 48);
            this.button5.TabIndex = 7;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_xbox_x_43;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button4.Location = new System.Drawing.Point(477, 55);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(44, 52);
            this.button4.TabIndex = 6;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // FUN_LIST
            // 
            this.FUN_LIST.FlatAppearance.BorderSize = 0;
            this.FUN_LIST.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FUN_LIST.Font = new System.Drawing.Font("Lucida Fax", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FUN_LIST.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_marcador_duplo_45;
            this.FUN_LIST.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.FUN_LIST.Location = new System.Drawing.Point(17, 417);
            this.FUN_LIST.Name = "FUN_LIST";
            this.FUN_LIST.Size = new System.Drawing.Size(176, 60);
            this.FUN_LIST.TabIndex = 10;
            this.FUN_LIST.Text = "        DADOS";
            this.FUN_LIST.UseVisualStyleBackColor = true;
            this.FUN_LIST.Click += new System.EventHandler(this.FUN_LIST_Click);
            // 
            // Funcionario
            // 
            this.Funcionario.FlatAppearance.BorderSize = 0;
            this.Funcionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Funcionario.Font = new System.Drawing.Font("Lucida Fax", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Funcionario.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_contatos_50;
            this.Funcionario.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Funcionario.Location = new System.Drawing.Point(17, 351);
            this.Funcionario.Name = "Funcionario";
            this.Funcionario.Size = new System.Drawing.Size(176, 60);
            this.Funcionario.TabIndex = 9;
            this.Funcionario.Text = "        FUNCIONÁRIOS";
            this.Funcionario.UseVisualStyleBackColor = true;
            this.Funcionario.Click += new System.EventHandler(this.button1_Click_4);
            // 
            // BotaoCliente
            // 
            this.BotaoCliente.FlatAppearance.BorderSize = 0;
            this.BotaoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotaoCliente.Font = new System.Drawing.Font("Lucida Fax", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoCliente.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_crachá_50;
            this.BotaoCliente.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BotaoCliente.Location = new System.Drawing.Point(17, 219);
            this.BotaoCliente.Name = "BotaoCliente";
            this.BotaoCliente.Size = new System.Drawing.Size(176, 60);
            this.BotaoCliente.TabIndex = 8;
            this.BotaoCliente.Text = "         CLIENTE";
            this.BotaoCliente.UseVisualStyleBackColor = true;
            this.BotaoCliente.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // BotaoListar
            // 
            this.BotaoListar.FlatAppearance.BorderSize = 0;
            this.BotaoListar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotaoListar.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoListar.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_pesquisar_50;
            this.BotaoListar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BotaoListar.Location = new System.Drawing.Point(17, 87);
            this.BotaoListar.Name = "BotaoListar";
            this.BotaoListar.Size = new System.Drawing.Size(176, 60);
            this.BotaoListar.TabIndex = 7;
            this.BotaoListar.Text = "     LISTAR";
            this.BotaoListar.UseVisualStyleBackColor = true;
            this.BotaoListar.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // BotaoEstoque
            // 
            this.BotaoEstoque.FlatAppearance.BorderSize = 0;
            this.BotaoEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotaoEstoque.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BotaoEstoque.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_tablet_windows_8_48;
            this.BotaoEstoque.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BotaoEstoque.Location = new System.Drawing.Point(17, 285);
            this.BotaoEstoque.Name = "BotaoEstoque";
            this.BotaoEstoque.Size = new System.Drawing.Size(176, 60);
            this.BotaoEstoque.TabIndex = 6;
            this.BotaoEstoque.Text = "     ESTOQUE";
            this.BotaoEstoque.UseVisualStyleBackColor = true;
            this.BotaoEstoque.Click += new System.EventHandler(this.BotaoEstoque_Click);
            // 
            // botao3
            // 
            this.botao3.FlatAppearance.BorderSize = 0;
            this.botao3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botao3.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botao3.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_dinheiro_50;
            this.botao3.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.botao3.Location = new System.Drawing.Point(17, 153);
            this.botao3.Name = "botao3";
            this.botao3.Size = new System.Drawing.Size(176, 60);
            this.botao3.TabIndex = 5;
            this.botao3.Text = "         PAGAMENTO";
            this.botao3.UseVisualStyleBackColor = true;
            this.botao3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Botao
            // 
            this.Botao.BackColor = System.Drawing.Color.Pink;
            this.Botao.FlatAppearance.BorderSize = 0;
            this.Botao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Botao.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Botao.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_carrinho_de_compras_50;
            this.Botao.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Botao.Location = new System.Drawing.Point(17, 17);
            this.Botao.Name = "Botao";
            this.Botao.Size = new System.Drawing.Size(176, 60);
            this.Botao.TabIndex = 3;
            this.Botao.Text = "     PEDIDO";
            this.Botao.UseVisualStyleBackColor = false;
            this.Botao.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 504);
            this.Controls.Add(this.telaInicial2);
            this.Controls.Add(this.listarFuncionario1);
            this.Controls.Add(this.registrarFuncionario1);
            this.Controls.Add(this.cliente1);
            this.Controls.Add(this.telaInicial1);
            this.Controls.Add(this.listarPedido1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.estoque1);
            this.Controls.Add(this.contato1);
            this.Controls.Add(this.userControl12);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button Botao;
        private System.Windows.Forms.Button botao3;
        private System.Windows.Forms.Button button5;
        private UserControl1 userControl11;
        private System.Windows.Forms.Button BotaoEstoque;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BotaoListar;
        private System.Windows.Forms.Button BotaoCliente;

        private UserControl1 userControl12;
        private contato contato1;
        private Estoque estoque1;
        private Listartudo listarPedido1;
        private Cliente cliente1;
        private TelaInicial telaInicial1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Funcionario;
        private RegistrarFuncionario registrarFuncionario1;
        private System.Windows.Forms.Button FUN_LIST;
        private ListarFuncionario listarFuncionario1;
        private TelaInicial telaInicial2;
        private System.Windows.Forms.Button button4;
    }
}

