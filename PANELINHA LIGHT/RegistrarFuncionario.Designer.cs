﻿namespace PANELINHA_LIGHT
{
    partial class RegistrarFuncionario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtfuncionario = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtcargo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txttempotrab = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtsalario = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtdata = new System.Windows.Forms.TextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtfuncionario
            // 
            this.txtfuncionario.Location = new System.Drawing.Point(178, 15);
            this.txtfuncionario.Multiline = true;
            this.txtfuncionario.Name = "txtfuncionario";
            this.txtfuncionario.Size = new System.Drawing.Size(330, 30);
            this.txtfuncionario.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 26);
            this.label1.TabIndex = 7;
            this.label1.Text = "FUNCIONÁRIO";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtcargo
            // 
            this.txtcargo.Location = new System.Drawing.Point(178, 71);
            this.txtcargo.Multiline = true;
            this.txtcargo.Name = "txtcargo";
            this.txtcargo.Size = new System.Drawing.Size(330, 30);
            this.txtcargo.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(3, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 26);
            this.label3.TabIndex = 11;
            this.label3.Text = "CARGO";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txttempotrab
            // 
            this.txttempotrab.Location = new System.Drawing.Point(178, 127);
            this.txttempotrab.Multiline = true;
            this.txttempotrab.Name = "txttempotrab";
            this.txttempotrab.Size = new System.Drawing.Size(330, 30);
            this.txttempotrab.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 24);
            this.label2.TabIndex = 13;
            this.label2.Text = "TEMPO DE TRAB.";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtsalario
            // 
            this.txtsalario.Location = new System.Drawing.Point(178, 181);
            this.txtsalario.Multiline = true;
            this.txtsalario.Name = "txtsalario";
            this.txtsalario.Size = new System.Drawing.Size(330, 30);
            this.txtsalario.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(3, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 24);
            this.label4.TabIndex = 15;
            this.label4.Text = "SALÁRIO";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(1, 239);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 22);
            this.label5.TabIndex = 17;
            this.label5.Text = "DATA NASCIMENTO";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // txtdata
            // 
            this.txtdata.Location = new System.Drawing.Point(178, 235);
            this.txtdata.Multiline = true;
            this.txtdata.Name = "txtdata";
            this.txtdata.Size = new System.Drawing.Size(330, 30);
            this.txtdata.TabIndex = 18;
            // 
            // btnSalvar
            // 
            this.btnSalvar.FlatAppearance.BorderSize = 0;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = global::PANELINHA_LIGHT.Properties.Resources.icons8_alarmes_40;
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSalvar.Location = new System.Drawing.Point(168, 281);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(135, 60);
            this.btnSalvar.TabIndex = 19;
            this.btnSalvar.Text = "        SALVAR";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.Botao_Click);
            // 
            // RegistrarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.txtdata);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtsalario);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txttempotrab);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtcargo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtfuncionario);
            this.Controls.Add(this.label1);
            this.Name = "RegistrarFuncionario";
            this.Size = new System.Drawing.Size(511, 358);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtfuncionario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtcargo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txttempotrab;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtsalario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtdata;
        private System.Windows.Forms.Button btnSalvar;
    }
}
